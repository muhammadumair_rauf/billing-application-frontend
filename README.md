# Billing App Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.2.

## How To Run
* First install node dependencies by running the command. `npm install`. 
* Run `npm start` or `ng serve --ssl true --ssl-key server.key --ssl-cert server.crt` for a dev server and  Navigate to `https://localhost:4200/`. (Note this is https because facebook plugins not allow to request their app from http protocol). This will automatically redirect to you login page.
* If Facebbok register popup will not show first allow fb popup in your browser setting and then refresh page (Note: it is because of we are using https for fb on local server)


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
