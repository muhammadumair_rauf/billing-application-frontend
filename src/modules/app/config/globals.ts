export const GLOBALS = {
  appName: 'Billing App',
  pageActions: { create: 'create', view: 'view', update: 'update' },

  FRIEND_STATUS: {
    sent: { title: 'Friend Request Sent', value: 'sent' },
    friend: { title: 'Friend', value: 'friend' }
  }

};
