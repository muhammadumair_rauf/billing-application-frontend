import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService as AuthGuard } from './services';

import {
  LayoutComponent,
  PublicLayoutComponent,
} from '../layout/components';

let rootRedirectUrl = '';



/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: 'login',
    component: PublicLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: 'modules/public/public.module#PublicModule'
      }
    ]
  },
  {
    path: '',
    canActivate: [AuthGuard],
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: rootRedirectUrl,
        pathMatch: 'full'
      },

      {
        path: 'home',
        loadChildren: 'modules/home/home.module#HomeModule',

      },

    ]
  },

  { path: '**', redirectTo: '' }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutes { }
