import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CdkTableModule } from '@angular/cdk/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';


import {
  MatIconRegistry,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatCardModule,
  MatIconModule,
  MatListModule,
  MatPaginatorModule,
} from '@angular/material';
import 'hammerjs';
import { AppComponent } from './components';
import { AuthGuardService } from './services';
import { UserService } from '../home/services'

/**
 * Social Logins
 */
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { FacebookLoginProvider } from "angularx-social-login";
let config = new AuthServiceConfig([
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("619103941819699")
  },

]);

export function provideConfig() {
  return config;
}

/**************************************************** */

export const __IMPORTS = [
  CommonModule,
  BrowserModule,
  BrowserAnimationsModule,
  HttpModule,
  HttpClientModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  FormsModule,
  ReactiveFormsModule,
  CdkTableModule,
  MatSelectModule,
  MatMenuModule,
  MatCardModule,
  MatIconModule,
  MatListModule,
  MatPaginatorModule,
  MatFormFieldModule,
  FlexLayoutModule,
  MatProgressSpinnerModule,
  SocialLoginModule
];

export const __DECLARATIONS = [
  AppComponent,
];

export const __PROVIDERS = [MatIconRegistry, AuthGuardService, UserService,
  // For Social Logins
  {
    provide: AuthServiceConfig,
    useFactory: provideConfig
  }
];

export const __ENTRY_COMPONENTS = [];
