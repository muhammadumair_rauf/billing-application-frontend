import { UserService } from '../../home/services';
import { Router, CanActivate, ActivatedRoute } from '@angular/router';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    public router: Router,
    private userService: UserService,
    private activatedRoute: ActivatedRoute
  ) { }

  // Checking if user is logged in or not
  canActivate(): boolean {

    if (!this.isAuthenticated()) {
      this.router.navigate(['/login']);
    }
    return true;
  }

  // This will authenticate the user
  isAuthenticated(): boolean {
    if (localStorage.getItem('id') && localStorage.getItem('token') && localStorage.getItem('token') !== undefined) {
      return true;
    } else {
      return false;
    }
  }
}
