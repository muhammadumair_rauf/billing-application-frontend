import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';

@Injectable()
export class LayoutService {

  /**
   * App title
   */
  private setTitleSource = new Subject<string>();
  setTitle$ = this.setTitleSource.asObservable();



  /**
   * For displaying or hiding Sub navigation 
   */
  private subNavSource = new Subject<boolean>();
  subNav$ = this.subNavSource.asObservable();

  /**
   * Classic Layout Module Navigation
   */
  private initModuleNavSource = new Subject<any>();
  initModuleNav$ = this.initModuleNavSource.asObservable();

  /**
   * For displaying Flash message
   *
   */
  private flashMsgSource = new Subject<object>();
  flashMsg$ = this.flashMsgSource.asObservable();

  constructor() { }

  /**
   * Set Application Title
   * @param titleData string
   */
  setTitle(titleData: string) {
    this.setTitleSource.next(titleData);
  }



  /**
   * Display / hide sub navigation  sub nav
   * @param active boolean
   */
  subNav(active: boolean) {
    this.subNavSource.next(active);
  }

  /**
   * initialize module navigation
   *
   * @param moduleName string
   */
  initModuleNav(moduleName) {
    this.initModuleNavSource.next(moduleName);
  }

  /**
   * Show Flash message
   *
   * @param fmData object
   */
  flashMsg(fmData: object) {
    this.flashMsgSource.next(fmData);
  }


  /**
   * layout service is the part of mostly component
   * so for now we are using layout service to trigger side nav.
   *
   */
  private toggleSideNavSource = new Subject<boolean>();

  toggleSideNav$ = this.toggleSideNavSource.asObservable();

  public toggleSideNav(check: boolean) {
    this.toggleSideNavSource.next(check);
  }


}
