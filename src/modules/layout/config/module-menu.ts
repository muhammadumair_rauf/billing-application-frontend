import { HomeMenu } from '../../home/config/home-menu';

export const ModuleMenu = [
  { name: 'home', title: 'Home', url: ['/home'], icon: '', moduleMenu: HomeMenu },
];
