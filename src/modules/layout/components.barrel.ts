// Import components and services etc here
import { CommonModule } from '@angular/common';

import {
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatListModule,
  MatButtonModule,
  MatTooltipModule,
  MatIconModule,
  MatCheckboxModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutService } from './services';

import {
  LayoutComponent,
  PublicLayoutComponent,
  PublicHeaderComponent,
  ClassicLayoutComponent,
  ClassicHeaderComponent,
  ClassicLeftNavComponent,
  ClassicFooterComponent,
  FlashMessageComponent,
} from './components';

export const __IMPORTS = [
  CommonModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatListModule,
  MatButtonModule,
  MatTooltipModule,
  FlexLayoutModule,
  MatIconModule,
  MatCheckboxModule,
  MatProgressSpinnerModule
];

export const __DECLARATIONS = [
  LayoutComponent,
  PublicHeaderComponent,
  PublicLayoutComponent,
  ClassicLayoutComponent,
  ClassicHeaderComponent,
  ClassicLeftNavComponent,
  ClassicFooterComponent,
  FlashMessageComponent,
];

export const __PROVIDERS = [LayoutService];

export const __ENTRY_COMPONENTS = [];
