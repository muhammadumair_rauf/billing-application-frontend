import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from '../../../services';
import { ModuleMenu } from '../../../config/module-menu';

@Component({
  selector: 'layout-classic-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class ClassicHeaderComponent implements OnInit {

  public moduleMenu = ModuleMenu;
  public isShow: boolean = false;
  public username: string;
  public userId: number;

  public selectedModuleName: string;

  constructor(private router: Router, private layoutService: LayoutService) { }

  /**
   * ngOnInit()
   */
  ngOnInit() {


    this.username = localStorage.getItem('username');
    this.userId = +localStorage.getItem('id');

    // If page is refreshed then maintain the navigation state
    let urlAry = this.router.url.split('/');
    if (urlAry.length > 1) {
      if (urlAry[1] != 'profile') {
        let item = this.moduleMenu.find(o => o.name === urlAry[1]);
        if (item) {
          this.navigateAndLoadMenu(item);
        }

      }

    }
  }

  /**
   * logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['login']);
  }

  public navigateAndLoadMenu(item) {

    let moduleMenu = item['moduleMenu'];

    this.selectedModuleName = item['name'];

    this.layoutService.initModuleNav(moduleMenu);
  }


  emitShowSideNav() {
    this.isShow = true;
    this.layoutService.toggleSideNav(this.isShow);
  }
}
