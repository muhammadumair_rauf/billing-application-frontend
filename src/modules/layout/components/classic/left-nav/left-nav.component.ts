import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from '../../../services';
import { Subscription } from 'rxjs';


@Component({
  selector: 'layout-classic-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.css']
})
export class ClassicLeftNavComponent implements OnInit {

  public loaded = false;

  public clicked: string = 'dashboard';

  public layoutSubscription: Subscription;

  public moduleMenuItems: any[];

  constructor(
    private router: Router,
    private layoutService: LayoutService
  ) {
    this.layoutSubscription = this.layoutService.initModuleNav$.subscribe(moduleMenu => {

      if (moduleMenu) {
        this.assignModuleMenu(moduleMenu);
      }
    }
    );
  }

  /**
   * ngOnInit()
   */
  ngOnInit() {

    this.clicked = this.router.url;
  }

  private assignModuleMenu(moduleMenu) {

    this.moduleMenuItems = null;

    this.moduleMenuItems = moduleMenu;

    this.loaded = false;


    if (this.moduleMenuItems.length) {
      this.loaded = true;
    } else {
    }
  }



  /**
   * route links for main navigations
   * @param link String
   */
  public navigateToUrl(item) {
    this.clicked = item.title;
    this.router.navigate(item.url);
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.layoutSubscription.unsubscribe();
  }
}
