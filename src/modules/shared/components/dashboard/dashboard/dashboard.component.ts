import { Component, OnInit, Input } from '@angular/core';
import * as _ from "lodash";
import { PageAnimation } from '../../../helper';
import { LayoutService } from 'modules/layout/services';

@Component({
  selector: 'shared-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [PageAnimation]
})
export class DashboardComponent implements OnInit {

  @Input() moduleMenu;

  @Input() type = 'icon';

  public pageState = 'active';

  public menuItems = [];

  constructor(public layoutService: LayoutService) { }

  ngOnInit(): void {



    _.each(this.moduleMenu, (item) => {
      _.each(item['menuItems'], menu => {
        this.menuItems.push(menu);
      })
    })


  }
}
