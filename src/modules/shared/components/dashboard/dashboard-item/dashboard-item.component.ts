import { Component, Input } from '@angular/core';

/**
 * Dashboard widget link
 *
 * How to use?
 *
 * <shared-dashboard-item  [icon]="'list'" [label]="'my label'" [link]="['/link/to/']"></shared-dashboard-item>
 */
@Component({
  selector: 'shared-dashboard-item',
  templateUrl: './dashboard-item.component.html',
  styleUrls: ['./dashboard-item.component.css']
})
export class DashboardItemComponent {

  @Input() type = 'icon';
  @Input() icon: string;
  @Input() label: string;
  @Input() link;

  constructor() {}
}
