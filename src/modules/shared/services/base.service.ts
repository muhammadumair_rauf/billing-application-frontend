import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { TokenService } from '.';
import { Router } from '@angular/router';

/**
 * Base Service Class
 *
 * Responsible to server almost every service in entire project.
 *
 * @class BaseService
*/
@Injectable()
export class BaseService {
  protected errorCode: number;

  /**
   * Constructor
   *
   * @method constructor
   * @param http Http
   */
  constructor(protected http: Http) {
    this._setHeaders();
  }

  /**
   *
   * With each request we need to set token with header for validation
   *
   *
   * @method setHeaders
  */
  private _setHeaders() {

    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));

    return new RequestOptions({ headers: headers })

  }


  /**
   * Now we need more than headers we need query string / url parameters
   * So this method is responsible for to convert provided params objec to convert into
   * compatible request options params.
   *
   *
   * @method setHeaderOptions
   * @param params object
   */
  private _setRequestOptions(params) {

    let requestOptions = this._setHeaders();

    if (params) {

      let myParams = new URLSearchParams();

      Object.keys(params).forEach((key) => {
        myParams.append(key, params[key]);
      })

      requestOptions.params = myParams;

    }

    return requestOptions;
  }


  /**
   * Send http get request to server.
   *
   * @method __get
   * @param url string
   * @param params object
   */
  protected __get(url, params?) {

    let reqOptions = this._setRequestOptions(params);

    return this.http.get(`${environment.backendAPIURL}/${url}`, reqOptions).catch(this.handleError);
  }


  /**
   * Send http put request to server.
   *
   * @method __put
   * @param url string
   * @param putBody object
   */
  __put(url, putBody) {

    return this.http.put(`${environment.backendAPIURL}/${url}`, putBody, this._setHeaders()).catch(this.handleError);

  }


  /**
   * Send http post request to server
   *
   * @method __post
   * @param url string
   * @param postBody object
   */
  __post(url, postBody) {

    return this.http.post(`${environment.backendAPIURL}/${url}`, postBody, this._setHeaders()).catch(this.handleError);

  }


  /**
   * Send http delte request to server
   *
   * @method __delete
   * @param url string
   */
  __delete(url) {

    return this.http.delete(`${environment.backendAPIURL}/${url}`, this._setHeaders()).catch(this.handleError);

  }


  /**
   * Erro Handler
   *
   * @method handleError
   * @param error object
   */
  protected handleError(error) {

    let errorJson = error.json()

    if (environment.production == false) {
      console.log(errorJson);
    }


    return Observable.throw(errorJson || 'Server error');



  }
}
