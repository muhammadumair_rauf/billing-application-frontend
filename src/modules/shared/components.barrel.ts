// Import components and services etc here
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatFormFieldModule,
  MatIconModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatInputModule,
  MatCardModule,
  MatDividerModule,
  MatListModule,
  MatSidenavModule,
  MatProgressBarModule,
  MatTooltipModule,
} from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';
import { BaseService } from './services';

import {
  DashboardComponent,
  DashboardItemComponent,
  PageLoaderComponent,
  SmallOverlayComponent,
  MatCardLoaderComponent,
} from './components';

import { AutofocusDirective } from './directives';


export const __IMPORTS = [
  FormsModule,
  ReactiveFormsModule,
  MatFormFieldModule,
  MatIconModule,
  MatButtonModule,
  MatInputModule,
  FlexLayoutModule,
  MatCardModule,
  MatDividerModule,
  FlexLayoutModule,
  MatProgressSpinnerModule,
  MatListModule,
  MatSidenavModule,
  MatProgressBarModule,
  MatTooltipModule,
];

export const __DECLARATIONS = [
  DashboardComponent,
  DashboardItemComponent,
  PageLoaderComponent,
  AutofocusDirective,
  SmallOverlayComponent,
  MatCardLoaderComponent,
];

export const __PROVIDERS = [BaseService];
export const __ENTRY_COMPONENTS = [];
