import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent, SocialLoginComponent, RegFormComponent } from './components';

/**
 * Available routing paths
 */
const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'signupWithFB', component: SocialLoginComponent },
  { path: 'signup', component: RegFormComponent },
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutes { }
