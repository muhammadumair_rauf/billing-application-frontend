import 'hammerjs';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup } from '@angular/forms';

import { LoginService } from '../../../home/services';
import { Login } from '../../../home/models';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  public fg: FormGroup;


  public attributesLabels = Login.attributesLabels;

  public loaded = true;
  public loginInProgress = false;

  public authError: string;

  public authSuccess: string;


  constructor(private activatedRoute: Router, private loginService: LoginService, private fb: FormBuilder) { }

  ngOnInit() {
    this.fg = this.fb.group(new Login().validationRules());
  }

  /**
   * Login If user is valid
   * @param form NgForm
   */
  login(item: Login) {
    // Set it true to display spinner now this will become false in case of error or will remaint true untill route redirect.
    this.loginInProgress = true;

    this.loginService.login(item).subscribe(response => {

      if (response) {

        this.loginInProgress = false;
        this.activatedRoute.navigate(['/home']);

      }
    },
      error => {
        this.loginInProgress = false;
        this.authError = error;
      }
    );
  }


}
