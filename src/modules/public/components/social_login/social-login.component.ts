import { Component, OnInit } from '@angular/core';

import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.css'],
})
export class SocialLoginComponent implements OnInit {

  public user: SocialUser;
  private loggedIn: boolean;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {

    this.signInWithFB();
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });
  }

  /**
   *Sign In using FB
   *
   * @private
   * @memberof SocialLoginComponent
   */
  private signInWithFB(): void {

    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
      (fbRes) => {
      }
    ).catch(error => {
    })
  }


  /**
   *Sign Out
   *
   * @private
   * @memberof SocialLoginComponent
   */
  private signOut(): void {
    this.authService.signOut().then(
      (fbRes) => {
      }
    ).catch(error => {
    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    console.log('Destroy')
    this.signOut();
  }
}
