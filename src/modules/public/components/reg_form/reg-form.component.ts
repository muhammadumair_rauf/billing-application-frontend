import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

import { PageAnimation } from 'modules/shared/helper';
import { User } from 'modules/home/models';
import { UserService } from '../../../home/services/user.service';
import { MatSnackBar } from '@angular/material';



@Component({
  selector: 'public-reg-form',
  templateUrl: './reg-form.component.html',
  styleUrls: ['./reg-form.component.css'],
  animations: [PageAnimation],
})
export class RegFormComponent implements OnInit {

  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded: boolean = true;
  public fg: FormGroup;
  public pageTitle: string;
  public user: User;
  public options: Observable<string[]>;
  @Input() fbUser;
  public userLabels = User.attributesLabels;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private userService: UserService,
    public snackBar: MatSnackBar
  ) {
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  private initializePage() {
    this.initAddPage();

  }

  private initAddPage() {
    this.user = new User();
    this.fg = this.fb.group(new User().createValidationRules());

    this.fg.enable();
    if (this.fbUser) {

      this.fg.patchValue({ username: this.fbUser.email, name: this.fbUser.name });

    }
    this.loaded = true;
  }


  /**
   *Save data
   *
   * @param {User} item
   * @memberof RegFormComponent
   */
  public saveData(item: User) {

    this.boxLoaded = false;

    if (this.fbUser) {
      item['fbLogin'] = true;
    } else {
      item['fbLogin'] = false;

    }

    this.create(item);

  }

  /**
   *Show snack bar
   *
   * @param {string} message
   * @param {string} action
   * @memberof RegFormComponent
   */
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  /**
   *Will create user
   *
   * @private
   * @param {User} item
   * @memberof RegFormComponent
   */
  private create(item: User) {

    this.userService.create(item).subscribe(
      response => {
        this.openSnackBar('User has been created.', 'Success');
        this.router.navigate([`/login`]);


      },
      error => {
        if (error.status == 403) {
          this.fg.controls['username'].setErrors({ exists: true });
        } else {
          this.openSnackBar('Something went wrong, please try again.', 'Error');
        }

      },
      () => {
        this.loaded = true;
        this.boxLoaded = true;
      }
    );
  }

}






