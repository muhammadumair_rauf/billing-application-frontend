// Import components and services etc here
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatFormFieldModule, MatIconModule, MatInputModule, MatButtonModule, MatCardModule, MatSelectModule, MatSnackBarModule } from '@angular/material';

import { LoginComponent, SocialLoginComponent, RegFormComponent } from './components';

export const __IMPORTS = [
  CommonModule,
  FormsModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  ReactiveFormsModule,
  MatSelectModule,
  FlexLayoutModule,
  MatSnackBarModule

];

export const __DECLARATIONS = [LoginComponent, SocialLoginComponent, RegFormComponent];

export const __PROVIDERS = [];

export const __ENTRY_COMPONENTS = [];
