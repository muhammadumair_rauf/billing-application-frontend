import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class Login {

  username: string;
  password: string;

  /**
   * Attributes Labels
   */
  static attributesLabels = {
    username: 'Username',
    password: 'Password',
  };

  /** 
   * Construstor
  */
  constructor() { }


  /** 
   * Form Validation Rules
   * 
  */
  public validationRules() {
    return {
      username: new FormControl('', [<any>Validators.required, Validators.email, Validators.maxLength(75)]),
      password: new FormControl('', [<any>Validators.required, Validators.minLength(6), Validators.maxLength(75)]),
    };
  }
  /** 
   * Forgot Password Validation Rules
   * 
  */
  public FPValidationRules() {
    return {
      username: new FormControl('', [<any>Validators.required, Validators.email, Validators.maxLength(30)]),
    };
  }
}
