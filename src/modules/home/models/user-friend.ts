import { User } from "./user";

export class UserFreind {

  public id?: number;
  public senderId?: number;
  public recieverId?: number;
  public status?: string;
  public sender?: User;
  public reciever?: User;
  public senderName?: string;
  public senderUsername?: string;
  public friendId?: number;


  constructor() { }

}
