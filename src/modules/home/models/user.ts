import { FormControl, Validators, ValidatorFn, FormGroup, AbstractControl } from '@angular/forms';

export class User {
  static attributesLabels = {
    username: 'Email',
    name: 'Name',
    password: 'Password',
    confirmPassword: 'Confirm Password',

  };

  public id?: number;
  public name?: string;
  public username: string;
  public password?: string;
  public confirmPassword?: string;
  public fbLogin?: boolean;

  constructor() { }

  /**
    *
    * @param equalControlName
    */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      if (!control['_parent']) return null;

      if (!control['_parent'].controls[equalControlName]) throw new TypeError('Form Control ' + equalControlName + ' does not exists.');

      let controlMatch = control['_parent'].controls[equalControlName];

      // cehecking the field is dirty
      if (controlMatch.dirty === true) {
        if (controlMatch.value == control.value) {
          controlMatch.setErrors(null);
          return null;
        } else {

          return { equalTo: true };
        }
      }

    };
  }

  /**
   * Create Form Validation Rules
   */
  public createValidationRules?() {
    return {
      username: new FormControl('', [Validators.required, Validators.email]),
      name: new FormControl('', Validators.required),

      password: new FormControl('', [
        Validators.required,
        Validators.maxLength(30),
        this.equalTo('confirmPassword')
      ]),
      confirmPassword: new FormControl('', [
        Validators.required,
        Validators.maxLength(30),
        this.equalTo('password')
      ]),
    };
  }


}
