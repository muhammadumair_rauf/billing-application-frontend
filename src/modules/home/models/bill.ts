import { FormControl, Validators, ValidatorFn, FormGroup, AbstractControl } from '@angular/forms';

export class Bill {
  static attributesLabels = {
    userIds: 'Friends',
    title: 'Title',
    amount: 'Amount',

  };

  public id?: number;
  public title?: string;
  public amount?: number;

  constructor() { }

  /**
  * Checking validation of number
  * @param equalControlName
  */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      let controlMatch = 0;
      return controlMatch <= control.value && 9999999999 >= control.value
        ? null
        : {
          negative: true
        };
    };
  }

  /**
   * Create Form Validation Rules
   */
  public createValidationRules?() {
    return {
      userIds: new FormControl([], [Validators.required]),
      title: new FormControl('', Validators.required),
      amount: new FormControl('', [Validators.required, this.greaterThan('amount')]),
    };
  }


}
