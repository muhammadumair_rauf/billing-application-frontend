import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  UserListComponent,
  DashboardComponent,
  FriendRequestListComponent,
  FriendListComponent,
  BillListComponent,
  BillFormComponent
} from './components';
import { AuthGuardService } from '../app/services';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardService],
    component: DashboardComponent,
  },
  {
    path: 'users',
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: UserListComponent,
      },
    ]
  },
  {
    path: 'friendRequests',
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: FriendRequestListComponent,
      },
    ]
  },
  {
    path: 'friends',
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: FriendListComponent,
      },
    ]
  },
  {
    path: 'bills',
    children: [
      {
        path: '',
        canActivate: [AuthGuardService],
        component: BillListComponent
      },
      {
        path: 'create',
        canActivate: [AuthGuardService],
        component: BillFormComponent
      },
    ]
  }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutes { }
