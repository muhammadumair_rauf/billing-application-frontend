export const HomeMenu = [
  {
    heading: 'Home',
    menuItems: [
      { title: 'People', url: ['/home/users'], icon: 'supervisor_account' },
      { title: 'Friend Requests', url: ['/home/friendRequests'], icon: 'person_add' },
      { title: 'Friends', url: ['/home/friends'], icon: 'face' },
      { title: 'Bill', url: ['/home/bills'], icon: 'chrome_reader_mode' },
    ]
  }
];
