import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BaseService } from '../../shared/services';
import { Bill } from '../models/bill';

@Injectable()
export class BillService extends BaseService {
  private routeURL: String = 'home/bills';

  constructor(protected http: Http) {
    super(http)
  }


  create(userFreind: Bill) {
    return this.__post(`${this.routeURL}/create`, userFreind).map(data => {
      return data.json();
    });
  }
  list() {
    return this.__get(`${this.routeURL}/list`).map(data => {
      return data.json();
    });
  }



}
