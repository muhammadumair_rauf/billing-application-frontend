import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

// import { BaseService } from 'modules/shared/services';
import { BaseService } from '../../shared/services';

import { Login } from '../models';

@Injectable()
export class LoginService extends BaseService {
  private routeURL: String = 'home/users';
  constructor(protected http: Http) {
    super(http)
  }

  login(item: Login): Observable<boolean> {
    return this.__post(`${this.routeURL}/login`, item)
      .map(response => {
        let res = response.json();

        localStorage.setItem('id', res['id']);
        localStorage.setItem('username', res['username']);
        localStorage.setItem('token', res['token']);


        return res['username'];
        // return true;
      });
  }
}
