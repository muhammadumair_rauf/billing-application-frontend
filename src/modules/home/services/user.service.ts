import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BaseService } from '../../shared/services';
import { User } from '../models';

@Injectable()
export class UserService extends BaseService {
  private routeURL: String = 'home/users';

  constructor(protected http: Http) {
    super(http)
  }

  list(id: number) {
    return this.__get(`${this.routeURL}/list/${id}`).map(data => {
      return data.json();
    });
  }


  create(user: User) {
    return this.__post(`${this.routeURL}/create`, user).map(data => {
      return data.json();
    });
  }


  login(user: User) {
    return this.__post(`${this.routeURL}/login`, user).map(data => {
      return data.json();
    });
  }
}
