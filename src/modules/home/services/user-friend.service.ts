import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { BaseService } from '../../shared/services';
import { UserFreind } from '../models/user-friend';

@Injectable()
export class UserFriendService extends BaseService {
  private routeURL: String = 'home/userFriends';

  constructor(protected http: Http) {
    super(http)
  }


  create(userFreind: UserFreind) {
    return this.__post(`${this.routeURL}/create`, userFreind).map(data => {
      return data.json();
    });
  }

  findFriends(id: number, status: string) {
    return this.__get(`${this.routeURL}/findFriends/${id}/${status}`).map(data => {
      return data.json();
    });
  }
  findFriendRequests(id: number, status: string) {
    return this.__get(`${this.routeURL}/findFriendRequests/${id}/${status}`).map(data => {
      return data.json();
    });
  }
  friendRequestAction(id: number, status: string) {
    return this.__get(`${this.routeURL}/friendRequestAction/${id}/${status}`).map(data => {
      return data.json();
    });
  }


}
