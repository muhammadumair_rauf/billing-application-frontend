import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

import { PageAnimation } from 'modules/shared/helper';
import { LayoutService } from 'modules/layout/services';
import { UserService, UserFriendService } from '../../services';
import { User } from '../../models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'home-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  animations: [PageAnimation]
})
export class UserListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public users: User[] = [new User()];

  public attrLabels = User.attributesLabels;

  public displayedColumns = ['name', 'username', 'options'];
  public dataSource: any;
  public FRIEND_STATUS = GLOBALS.FRIEND_STATUS;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;



  constructor(
    public layoutService: LayoutService,
    private userService: UserService,
    private userFriendService: UserFriendService,
  ) { }

  ngOnInit(): void {
    this.getRecords();
  }

  /**
   *Get list of people ie user
   *
   * @private
   * @memberof UserListComponent
   */
  private getRecords(): void {

    this.userService.list(+localStorage.getItem('id')).subscribe(response => {

      this.users = response;
      this.dataSource = new MatTableDataSource<User>(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';

      }

    );
  }

  /**
   *Filter on user
   *
   * @param {string} filterValue
   * @memberof UserListComponent
   */
  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /**
   *This will sort data
   *
   * @memberof UserListComponent
   */
  public sortData() {
    this.dataSource.sort = this.sort;
  }

  /**
   *Will send the friend requests
   *
   * @param {number} recieverId
   * @memberof UserListComponent
   */
  public sendFriendRequest(recieverId: number): void {

    let item = { senderId: +localStorage.getItem('id'), recieverId: recieverId, status: this.FRIEND_STATUS.sent.value }
    this.userFriendService.create(item).subscribe(response => {
      this.layoutService.flashMsg({ msg: 'Friend Request Sent', msgType: 'success' });
      this.getRecords();

    },
      error => {
        console.log(error);
      },

    );
  }
}
