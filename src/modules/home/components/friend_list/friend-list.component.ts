import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

import { PageAnimation } from 'modules/shared/helper';
import { LayoutService } from 'modules/layout/services';
import { UserFriendService } from '../../services';
import { User, UserFreind } from '../../models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'home-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.css'],
  animations: [PageAnimation]
})
export class FriendListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public users: UserFreind[] = [];

  public attrLabels = User.attributesLabels;

  public displayedColumns = ['name', 'username'];
  public dataSource: any;
  public FRIEND_STATUS = GLOBALS.FRIEND_STATUS;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  /**
   *Creates an instance of FriendListComponent.
   * @param {LayoutService} layoutService
   * @param {UserFriendService} userFriendService
   * @memberof FriendListComponent
   */
  constructor(
    public layoutService: LayoutService,
    private userFriendService: UserFriendService,
  ) { }

  ngOnInit(): void {
    this.getRecords();
  }

  private getRecords(): void {

    this.userFriendService.findFriends(+localStorage.getItem('id'), this.FRIEND_STATUS.friend.value).subscribe(response => {

      this.users = response;
      this.users.map(element => {
        element.senderName = ((element['sender']['id'] === +localStorage.getItem('id')) ? element['reciever']['name'] : element['sender']['name']);
        element.senderUsername = ((element['sender']['id'] === +localStorage.getItem('id')) ? element['reciever']['username'] : element['sender']['username']);
      })
      this.dataSource = new MatTableDataSource<UserFreind>(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';

      }

    );
  }


  /**
   *Apply filter
   *
   * @param {string} filterValue
   * @memberof FriendListComponent
   */
  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /**
   *Sort list Data
   *
   * @memberof FriendListComponent
   */
  public sortData() {
    this.dataSource.sort = this.sort;
  }


}
