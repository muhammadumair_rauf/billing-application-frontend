import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormBuilder } from '@angular/forms';

import { PageAnimation } from 'modules/shared/helper';
import { LayoutService } from 'modules/layout/services';
import { UserFriendService } from '../../services';
import { User, UserFreind } from '../../models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'home-friend-request-list',
  templateUrl: './friend-request-list.component.html',
  styleUrls: ['./friend-request-list.component.css'],
  animations: [PageAnimation]
})
export class FriendRequestListComponent implements OnInit {

  public pageState;

  public loaded: boolean = false;

  public users: UserFreind[] = [];

  public attrLabels = User.attributesLabels;

  public displayedColumns = ['name', 'username', 'options'];
  public dataSource: any;
  public FRIEND_STATUS = GLOBALS.FRIEND_STATUS;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  /**
   *Creates an instance of FriendRequestListComponent.
   * @param {LayoutService} layoutService
   * @param {UserFriendService} userFriendService
   * @memberof FriendRequestListComponent
   */
  constructor(
    public layoutService: LayoutService,
    private userFriendService: UserFriendService,
  ) { }

  ngOnInit(): void {
    this.getRecords();
  }

  /**
   *Get records list of friend request
   *
   * @private
   * @memberof FriendRequestListComponent
   */
  private getRecords(): void {

    this.userFriendService.findFriendRequests(+localStorage.getItem('id'), this.FRIEND_STATUS.sent.value).subscribe(response => {

      this.users = response;
      this.users.map(element => {
        element.senderName = element['sender']['name'];
        element.senderUsername = element['sender']['username'];
      })
      this.dataSource = new MatTableDataSource<UserFreind>(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';

      }

    );
  }

  /**
   *Filter the list
   *
   * @param {string} filterValue
   * @memberof FriendRequestListComponent
   */
  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /**
   *This will sort the data
   *
   * @memberof FriendRequestListComponent
   */
  public sortData() {
    this.dataSource.sort = this.sort;
  }


  /**
   *Friend request action
   *
   * @param {number} id
   * @param {string} status
   * @memberof FriendRequestListComponent
   */
  public friendRequestAction(id: number, status: string): void {

    this.userFriendService.friendRequestAction(id, status).subscribe(response => {
      if (status === this.FRIEND_STATUS.friend.value) {
        this.layoutService.flashMsg({ msg: 'Friend Request Approved', msgType: 'success' });
      } else {
        this.layoutService.flashMsg({ msg: 'Friend Request Reject', msgType: 'success' });
      }

      this.getRecords();

    },
      error => {

      },
      () => {

      }

    );
  }
}
