import { Component, OnInit } from '@angular/core';

import { HomeMenu } from '../../config/home-menu';

@Component({
  selector: 'academic-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {

  public moduleMenu = HomeMenu;

  constructor() { }

  ngOnInit(): void {
  }
}
