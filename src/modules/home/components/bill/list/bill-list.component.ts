import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { PageAnimation } from 'modules/shared/helper';
import { GLOBALS } from 'modules/app/config/globals';
import { LayoutService } from 'modules/layout/services';
import { BillService } from '../../../services';
import { Bill } from '../../../models';

@Component({
  selector: 'home-bill-list',
  templateUrl: './bill-list.component.html',
  styleUrls: ['./bill-list.component.css'],
  animations: [PageAnimation]
})
export class BillListComponent implements OnInit {
  public pageState;

  public loaded: boolean = false;

  public data: Bill[] = [];

  public attrLabels = Bill.attributesLabels;

  displayedColumns = ['title', 'amount', 'amountPer', 'participant'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;



  constructor(private billService: BillService, private router: Router, public layoutService: LayoutService) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  private getRecords() {
    this.billService.list().subscribe(
      response => {
        if (response) {
          this.data = response;
          this.dataSource = new MatTableDataSource<Bill>(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /**
   *Sort list data
   *
   * @memberof BillListComponent
   */
  public sortData() {
    this.dataSource.sort = this.sort;
  }
}
