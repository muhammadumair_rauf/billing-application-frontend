import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PageAnimation } from 'modules/shared/helper';
import { GLOBALS } from 'modules/app/config/globals';

import { BillService, UserFriendService } from '../../../services';
import { LayoutService } from 'modules/layout/services';

import { Bill, UserFreind } from '../../../models';



@Component({
  selector: 'home-bill-form',
  templateUrl: './bill-form.component.html',
  styleUrls: ['./bill-form.component.css'],
  animations: [PageAnimation]
})
export class BillFormComponent implements OnInit {
  public pageState = 'active';

  public loaded: boolean = false;
  public boxLoaded: boolean = true;


  public fg: FormGroup;
  public bill: Bill;
  public componentLabels = Bill.attributesLabels;
  public FRIEND_STATUS = GLOBALS.FRIEND_STATUS;
  public users: UserFreind[] = [];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private billService: BillService,
    private userFriendService: UserFriendService,
    private layoutService: LayoutService,
  ) {
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.findFriends();
  }


  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new Bill().createValidationRules());

    this.initCreatePage();

  }


  /**
   * Init create page
   */
  private initCreatePage() {
    this.loaded = true;

    this.bill = new Bill();

    this.fg.enable();
  }


  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: Bill) {
    this.boxLoaded = false;
    item['userIds'].push(+localStorage.getItem('id'));
    this.billService.create(item).subscribe(
      response => {
        this.layoutService.flashMsg({ msg: 'Bill has been created.', msgType: 'success' });
        this.router.navigate([`/home/bills`]);
      },
      error => {
        this.layoutService.flashMsg({ msg: 'Something went wrong, please try again.', msgType: 'error' });
        console.log(error);
        this.boxLoaded = true;
      },
      () => {
        this.boxLoaded = true;
      }
    );

  }

  /**
   *Find Friends
   *
   * @private
   * @memberof BillFormComponent
   */
  private findFriends() {
    this.userFriendService.findFriends(+localStorage.getItem('id'), this.FRIEND_STATUS.friend.value).subscribe(response => {

      this.users = response;
      this.users.map(element => {
        element.senderName = ((element['sender']['id'] === +localStorage.getItem('id')) ? element['reciever']['name'] : element['sender']['name']);
        element.senderUsername = ((element['sender']['id'] === +localStorage.getItem('id')) ? element['reciever']['username'] : element['sender']['username']);
        element.friendId = ((element['sender']['id'] === +localStorage.getItem('id')) ? element['reciever']['id'] : element['sender']['id']);
      })
    },
      error => {
        this.loaded = true;
        this.pageState = 'active';
      },
      () => {
        this.loaded = true;
        this.pageState = 'active';

      }

    );
  }


}
