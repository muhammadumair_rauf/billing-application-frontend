// Import components and services etc here

import {
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatTableModule,
  MatPaginatorModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  MatSortModule,
  MatToolbarModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  UserService,
  UserFriendService,
  BillService
} from './services';

import {
  UserListComponent,
  DashboardComponent,
  FriendRequestListComponent,
  FriendListComponent,
  BillFormComponent,
  BillListComponent
} from './components';

export const __IMPORTS = [
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  FormsModule,
  ReactiveFormsModule,
  MatTableModule,
  MatPaginatorModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  FlexLayoutModule,
  MatSortModule,
  MatToolbarModule
];

export const __DECLARATIONS = [
  UserListComponent,
  DashboardComponent,
  FriendRequestListComponent,
  FriendListComponent,
  BillFormComponent,
  BillListComponent
];

export const __PROVIDERS = [
  UserService,
  UserFriendService,
  BillService
];

export const __ENTRY_COMPONENTS = [];
